# my-subl-config
This is my __Sublime Text 3__ config files. 

# Usage
+ Simply copy the cotent to your own config files.
+ You may also need to follow the [__Set Up__](https://chrislinn.github.io/2017/02/09/subl/#set-up) for compatibility purpose.

# License
This project is licensed under the MIT License.